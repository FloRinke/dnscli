import click
import logging
from tabulate import tabulate
from sccdnslib import query
import ipaddress

import json
from pygments import highlight
from pygments.lexers import javascript
from pygments.formatters import terminal

logger = logging.getLogger(__name__)


def pp_json(json_thing, sort=True, indents=4):
    if type(json_thing) is str:
        print(highlight(json.dumps(json.loads(json_thing),
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    else:
        print(highlight(json.dumps(json_thing,
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    return None


@click.group('record')
def record_cli():
    logger.info("Called objtype record")


@record_cli.command('api')
@click.pass_context
def show_api(context):
    logger.info("request index")
    result = query.dns_record_api(context.obj.cert)
    pp_json(result)


@record_cli.command('list')
@click.option('--domain', 'domain', default=None, help="Übergeordneter Domainname (FQDN)")
@click.option('--fqdn', 'fqdn', default=None,
              help="FQDN des Resource-Record-Sets (FQDN muss in einem RR bzw. RR-Set vorkommen)")
@click.option('--fqdn-inttype', 'fqdn_inttype', default=None,
              help="Name der systeminternen DNS-DB-Namenstypdefinition (DBNT)")
@click.option('--fqdn-regexp', 'fqdn_regexp', default=None,
              help="Regulärer Ausdruck des Suchmusters für Owner-FQDN's")
@click.option('--inttype', 'inttype', default=None,
              help="Name der systeminternen DNS-DB-Record-Typdefinition (DBRT)")
@click.option('--label', 'label', default=None,
              help="linker Teil des FQDN's bis zum ersten Punkt")
@click.option('--label-regexp', 'label_regexp', default=None,
              help="Regulärer Ausdruck des Suchmusters für FQDN-Labels")
@click.option('--global/--no-global', 'listopt_global', default=False,
              help="Suche nicht auf eigene Adressbereiche beschränken")
@click.option('--ttl/--no-ttl', 'listopt_nn_ttl', default=False,
              help="nur Resource-Records mit explizit spezifizierter TTL ausgeben")
@click.option('--subtree/--no-subtree', 'listopt_subtree', default=False,
              help="rekursive Ausgabe, falls 'domain' spezifiziert wurde")
@click.option('--range', 'range_param', default=None, help="Name des Adressbereiches")
@click.option('--target-fqdn', 'target_fqdn', default=None,
              help="DNS-FQDN des Resource-Record-Ziels (Ziel-FQDN muss in einem RR vorkommen)")
@click.option('--target-fqdn-inttype', 'target_fqdn_inttype', default=None,
              help="Bezeichnung des DNS-DB-Namenstyps des Ziel-FQDN's")
@click.option('--target-fqdn-regexp', 'target_fqdn_regexp',
              help="Regulärer Ausdruck des Suchmusters für Ziel-FQDN's")
@click.option('--target-net', 'target_ipaddr_cidr_mask', default=None,
              help="Netzmaske für Ziel-IP-Adresse in CIDR-Notation")
@click.option('--target-dhcp/--no-target-dhcp', 'target_ipaddr_is_dhcp', default=False,
              help="DHCP-Attribut für Ziel-IP-Adresse (Boolean)")
@click.option('--type', 'type_param', default=None, help="RR-Typname")
@click.option('--zone', 'zone', default=None, help="dns.zone.name")
@click.pass_context
def list_record(context,
                domain,
                fqdn,
                fqdn_inttype,
                fqdn_regexp,
                inttype,
                label,
                label_regexp,
                listopt_global,
                listopt_nn_ttl,
                listopt_subtree,
                range_param,
                target_fqdn,
                target_fqdn_inttype,
                target_fqdn_regexp,
                target_ipaddr_cidr_mask,
                target_ipaddr_is_dhcp,
                type_param,
                zone
                ):
    logger.info("Called func list")
    params = dict()

    if domain is not None:
        params['domain'] = domain
    if fqdn is not None:
        params['fqdn'] = fqdn
    if fqdn_inttype is not None:
        params['fqdn_inttype'] = fqdn_inttype
    if fqdn_regexp is not None:
        params['fqdn_regexp'] = fqdn_regexp
    if inttype is not None:
        params['inttype'] = inttype
    if label is not None:
        params['label'] = label
    if label_regexp is not None:
        params['label_regexp'] = label_regexp
    if listopt_global is True:
        params['listopt_global'] = listopt_global
    if listopt_nn_ttl is True:
        params['listopt_nn_ttl'] = listopt_nn_ttl
    if listopt_subtree is True:
        params['listopt_subtree'] = listopt_subtree
    if range_param is not None:
        params['range'] = range_param
    if target_fqdn is not None:
        params['target_fqdn'] = target_fqdn
    if target_fqdn_inttype is not None:
        params['target_fqdn_inttype'] = target_fqdn_inttype
    if target_fqdn_regexp is not None:
        params['target_fqdn_regexp'] = target_fqdn_regexp
    if target_ipaddr_cidr_mask is not None:
        params['target_ipaddr_cidr_mask'] = target_ipaddr_cidr_mask
    if target_ipaddr_is_dhcp is True:
        params['target_ipaddr_is_dhcp'] = target_ipaddr_is_dhcp
    if type_param is not None:
        params['type'] = type_param
    if zone is not None:
        params['zone'] = zone

    logger.debug("Using options %s", params)
    result = query.dns_record_list(context.obj.cert, params=params)
    print(tabulate(result, headers='keys'))


@record_cli.command('create')
@click.argument('rrparams', nargs=-1)
@click.option('--description', 'fqdn_description', default=None,
              help="Freitext-Info zum FQDN")
@click.option('--ttl', 'ttl', default=None, type=click.INT,
              help="expliziter TTL-Wert (time-to-live in Sekunden) dieses DNS-RR-Sets")
@click.pass_context
def create_record(context, rrparams, fqdn_description, ttl):
    logger.info("Called func create")
    optionals = dict()
    if fqdn_description is not None:
        optionals['fqdn_description'] = fqdn_description
    if ttl is not None:
        optionals['ttl'] = ttl

    params = list()
    current_param = list()
    for item in rrparams:
        try:
            ipaddress.ip_address(item)
            # no exception, is valid ipaddress
            if len(current_param) > 0:
                # and a name was given before
                current_param.append(item)
            else:
                raise Exception("Name must be given before IP address(es)", item)
        except ValueError:
            # item is not an ip address
            if len(current_param) >= 2:
                # a valid name/ip-tuple was detected before
                params.append(current_param)
                current_param = list()
            elif len(current_param) > 0:
                raise Exception("Found next name before IP", item)
            current_param.append(item)
    # check if last data set is complete
    if len(current_param) >= 2:
        # a valid name/ip-tuple was detected before
        params.append(current_param)

    logger.debug("Using options %s, optionals %s", params, optionals)
    result = query.dns_record_create(context.obj.cert,
                                     params=params, params_opt=optionals,
                                     pretend=context.obj.pretend)
    print(result)


@record_cli.command('delete')
@click.argument('rrparams', nargs=-1)
@click.option('--parent/--no-parent', 'do_del_pqdn', default=False,
              help="Lösche übergeordnete FQDN's des gleichen Namenstyps automatisch, solange leer und nicht Zone Apex")
@click.option('--force', 'force_del_ref_records', default=None, type=click.INT,
              help="Lösche alle den FQDN referenzierenden DNS-Resource-Records automatisch mit.")
@click.pass_context
def delete_record(context, rrparams, do_del_pqdn, force_del_ref_records):
    logger.info("Called func delete")
    optionals = dict()
    if do_del_pqdn is True:
        optionals['do_del_pqdn'] = do_del_pqdn
    if force_del_ref_records is True:
        optionals['force_del_ref_records'] = force_del_ref_records

    params = list()
    current_param = list()
    for item in rrparams:
        try:
            ipaddress.ip_address(item)
            # no exception, is valid ipaddress
            if len(current_param) > 0:
                # and a name was given before
                current_param.append(item)
            else:
                raise Exception("Name must be given before IP address(es)", item)
        except ValueError:
            # item is not an ip address
            if len(current_param) >= 2:
                # a valid name/ip-tuple was detected before
                params.append(current_param)
                current_param = list()
            elif len(current_param) > 0:
                raise Exception("Found next name before IP", item)
            current_param.append(item)
    # check if last data set is complete
    if len(current_param) >= 2:
        # a valid name/ip-tuple was detected before
        params.append(current_param)

    logger.debug("Using options %s, optionals %s", params, optionals)
    result = query.dns_record_delete(context.obj.cert,
                                     params=params, params_opt=optionals,
                                     pretend=context.obj.pretend)
    print(result)
