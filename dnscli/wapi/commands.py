#!/usr/bin/env
import json
import logging

import click
import os
from pygments import highlight
from pygments.formatters import terminal
from pygments.lexers import javascript

from sccdnslib import query
from dnscli.wapi.datadict import commands as cmd_datadict

logger = logging.getLogger(__name__)


def pp_json(json_thing, sort=True, indents=4):
    if type(json_thing) is str:
        print(highlight(json.dumps(json.loads(json_thing),
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    else:
        print(highlight(json.dumps(json_thing,
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    return None


class Options(object):
    def __init__(self, cert, version, pretend=False):
        self.cert = cert
        self.version = version
        self.pretend = pretend


@click.group()
@click.option('--cert', type=click.Path(exists=True, file_okay=True, readable=True),
              default=os.path.join(os.path.expanduser("~"), 'dnscli.pem'), envvar='DNSCLI_CERT',
              help="Pfad des Client-Zertifikats")
@click.option('--version', default="2.0", help="Wähle API-Version")
@click.option('-p', '--pretend', is_flag=True, help="Befehl erzeugen und ausgeben, aber nicht abschicken")
@click.pass_context
def wapi(context, cert, version, pretend):
    logger.info("system wapi")
    context.obj = Options(cert, version, pretend)


wapi.add_command(cmd_datadict.datadict_cli)


@wapi.command('api')
@click.pass_context
def wapi_api(context):
    logger.info("request index")
    result = query.wapi_api(context.obj.cert)
    pp_json(result)


if __name__ == '__main__':
    logger.info("Run via wapicli")
    wapi(obj={})
