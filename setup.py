# sccdnslib - provide python interface to KIT-SCC's WebAPI
# Copyright (C) 2017  Florian Rinke
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

setup(
    name='dnscli',
    description='A cli-tool for accessing KIT-SCCs-WebAPI via sccdnslib',
    version='0.1',
    url='http://github.com/FloRinke/dnscli',
    author='Florian Rinke',
    author_email='github@florianrinke.de',
    license='AGPLv3',
    packages=find_packages(),
    py_modules=['dnscli.cli'],
    install_requires=[
        'Click',
        'tabulate',
        'sccdnslib',
    ],
    dependency_links=[
        "https://gitlab.com/FloRinke/sccdnslib"
    ],
    entry_points='''
        [console_scripts]
        dnscli=dnscli.dns.commands:dns
        wapicli=dnscli.wapi.commands:wapi
        allcli=dnscli.cli:cli
    ''',
)
