import click
import logging
from sccdnslib import query

import json
from pygments import highlight
from pygments.lexers import javascript
from pygments.formatters import terminal


logger = logging.getLogger(__name__)


def pp_json(json_thing, sort=True, indents=4):
    if type(json_thing) is str:
        print(highlight(json.dumps(json.loads(json_thing),
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    else:
        print(highlight(json.dumps(json_thing,
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    return None


@click.group('datadict')
def datadict_cli():
    logger.info("Called objtype datadict")


@datadict_cli.command('api')
@click.pass_context
def show_api(context):
    result = query.wapi_datadict_api(context.obj.cert)
    pp_json(result)


@datadict_cli.command('list')
@click.option('--diff', 'diff_version', default=None,
              help="num. Version <maj>.<min>, deren Unterschiede zur aktuellen Version ausgegeben werden sollen")
@click.option('--function', 'function_name', default=None,
              help="Name der WebAPI-Funktion")
@click.option('--ot', 'ot_name', default=None,
              help="WebAPI-Objekttypbezeichnung (TAB)")
@click.option('--ot-regex', 'ot_name_regexp', default=None,
              help="Regulärer Ausdruck des Suchmusters für Objekttyp-Namen")
@click.option('--param', 'param_name', default=None,
              help="wapi.param.name")
@click.option('--param-regex', 'param_name_regexp', default=None,
              help="Regulärer Ausdruck des Suchmusters für Parameter-Namen")
@click.option('--strict/--no-strict', 'strict_mode', default=True,
              help="Ausgabemodus für diff_version. 'true': vollständig; 'false': nur nicht-leer, geändert")
@click.option('--sys', 'sys_name', default=None,
              help="WAPI-Systembezeichnung (PKG)")
@click.option('--sys-regex', 'sys_name_regexp', default=None,
              help="Regulärer Ausdruck des Suchmusters für Objekttyp-Namen")
@click.pass_context
def list_datadict(context,
                  diff_version,
                  function_name,
                  ot_name,
                  ot_name_regexp,
                  param_name,
                  param_name_regexp,
                  strict_mode,
                  sys_name,
                  sys_name_regexp
                  ):
    logger.info("Called func list")
    params = dict()
    if diff_version is not None:
        params['diff_version'] = diff_version
    if function_name is not None:
        params['function_name'] = function_name
    if ot_name is not None:
        params['ot_name'] = ot_name
    if ot_name_regexp is not None:
        params['ot_name_regexp'] = ot_name_regexp
    if param_name is not None:
        params['param_name'] = param_name
    if param_name_regexp is not None:
        params['param_name_regexp'] = param_name_regexp
    if strict_mode is True:
        params['strict_mode'] = strict_mode
    if sys_name is not None:
        params['sys_name'] = sys_name
    if sys_name_regexp is not None:
        params['sys_name_regexp'] = sys_name_regexp
    result = query.wapi_datadict_list(context.obj.cert, params=params)
    pp_json(result)


@datadict_cli.command('ds')
@click.option('--diff', 'diff_version', default=None,
              help="num. Version <maj>.<min>, deren Unterschiede zur aktuellen Version ausgegeben werden sollen")
@click.option('--name', 'function_name', default=None,
              help="Name der WebAPI-Funktion")
@click.option('--strict/--no-strict', 'strict_mode', default=True,
              help="Ausgabemodus für diff_version. 'true': vollständig; 'false': nur nicht-leer, geändert")
@click.pass_context
def list_datadict_datastructure(context,
                                diff_version,
                                function_name,
                                strict_mode):
    logger.info("Called func list_datastructure")
    params = dict()
    if diff_version is not None:
        params['diff_version'] = diff_version
    if function_name is not None:
        params['function_name'] = function_name
    if strict_mode is True:
        params['strict_mode'] = strict_mode
    result = query.wapi_datadict_listds(context.obj.cert)
    pp_json(result)
