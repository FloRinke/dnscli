import click
import logging
from tabulate import tabulate
from sccdnslib import query


logger = logging.getLogger(__name__)


@click.group('ipaddr')
def ipaddr_cli():
    logger.info("Called objtype ipaddr")


@ipaddr_cli.command('list')
@click.option('--dhcp/--no-dhcp', 'is_dhcp', default=False)
@click.option('--global/--no-global', 'listopt_global', default=False)
@click.option('--max/--no-max', 'listopt_max_dns_rr_count', default=False)
@click.option('--min/--no-min', 'listopt_min_dns_rr_count', default=False)
@click.option('--range', 'range_param')
@click.option('--subnet', 'subnet_cidr_mask')
@click.pass_context
def list_ipaddr(context,
                is_dhcp,
                listopt_global,
                listopt_max_dns_rr_count,
                listopt_min_dns_rr_count,
                range_param,
                subnet_cidr_mask):
    logger.info("Called func list")
    params = dict()

    if is_dhcp is True:
        params['is_dhcp'] = is_dhcp
    if listopt_global is True:
        params['listopt_global'] = listopt_global
    if listopt_max_dns_rr_count is True:
        params['listopt_max_dns_rr_count'] = listopt_max_dns_rr_count
    if listopt_min_dns_rr_count is True:
        params['listopt_min_dns_rr_count'] = listopt_min_dns_rr_count
    if range_param is not None:
        params['range'] = range_param
    if subnet_cidr_mask is not None:
        params['subnet_cidr_mask'] = subnet_cidr_mask

    logger.debug("Using options %s", params)
    data = query.dns_ipaddr_list(context.obj.cert, params=params)
    print(tabulate(data, headers='keys'))
