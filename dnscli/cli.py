#!/usr/bin/env python3
import click
import logging
import sys

from dnscli.dns import commands as cmd_dns
from dnscli.wapi import commands as cmd_wapi

logger = logging.getLogger(__name__)


@click.group()
@click.option('--log', type=click.Choice(['debug', 'info', 'warning', 'error', 'critical']))
def cli(log):
    if log is not None:
        print("logging activate")
        if log == 'debug':
            lv = logging.DEBUG
        elif log == 'info':
            lv = logging.INFO
        elif log == 'warning':
            lv = logging.WARNING
        elif log == 'error':
            lv = logging.ERROR
        elif log == 'critical':
            lv = logging.CRITICAL
        else:
            lv = None
        logging.basicConfig(stream=sys.stderr, level=lv)


cli.add_command(cmd_dns.dns)
cli.add_command(cmd_wapi.wapi)

if __name__ == '__main__':
    logger.info("Run via allcli")
    cli()
