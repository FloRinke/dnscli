#!/usr/bin/env
import logging
import json

import click
import os
from pygments import highlight
from pygments.formatters import terminal
from pygments.lexers import javascript


from sccdnslib import query
from dnscli.dns.ipaddr import commands as cmd_ipaddr
from dnscli.dns.range import commands as cmd_range
from dnscli.dns.record import commands as cmd_record

logger = logging.getLogger(__name__)


def pp_json(json_thing, sort=True, indents=4):
    if type(json_thing) is str:
        print(highlight(json.dumps(json.loads(json_thing),
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    else:
        print(highlight(json.dumps(json_thing,
                                   sort_keys=sort,
                                   indent=indents),
                        javascript.JavascriptLexer(),
                        terminal.TerminalFormatter()))
    return None


class Options(object):
    def __init__(self, cert, version, pretend=False):
        self.cert = cert
        self.version = version
        self.pretend = pretend


@click.group()
@click.option('--cert', type=click.Path(exists=True, file_okay=True, readable=True),
              default=os.path.join(os.path.expanduser("~"), 'dnscli.pem'), envvar='DNSCLI_CERT',
              help="Pfad des Client-Zertifikats")
@click.option('--version', default="2.0", help="Wähle API-Version")
@click.option('-p', '--pretend', is_flag=True, help="Befehl erzeugen und ausgeben, aber nicht abschicken")
@click.pass_context
def dns(context, cert, version, pretend):
    logger.info("system dns")
    context.obj = Options(cert, version, pretend)


dns.add_command(cmd_ipaddr.ipaddr_cli)
dns.add_command(cmd_range.range_cli)
dns.add_command(cmd_record.record_cli)


@dns.command('api')
@click.pass_context
def dns_api(context):
    logger.info("request index")
    result = query.dns_record_api(context.obj.cert)
    pp_json(result)


if __name__ == '__main__':
    logger.info("Run via dnscli")
    dns(obj={})
