import click
import logging
from tabulate import tabulate
from sccdnslib import query


logger = logging.getLogger(__name__)


@click.group('range')
def range_cli():
    logger.info("Called objtype range")


@range_cli.command('list')
@click.option('--filter-zero/--no-filter-zero', 'filter_subnet_zero_mask', default=False,
              help="Bereichs-Subnetze mit /0 ausschließen")
@click.option('--broadcast/--no-broadcast', 'listopt_bcds', default=False,
              help="Broadcastdomain/Vlan-Liste pro Bereich ausgeben")
@click.option('--domains/--no-domains', 'listopt_domains', default=False,
              help="Domainliste pro Bereich ausgeben")
@click.option('--gateways/--no-gateways', 'listopt_gateways', default=False,
              help="Gatewayliste (IP-Adressen der Default-Router) pro Bereich ausgeben")
@click.option('--global/--no-global', 'listopt_global', default=False,
              help="Suche nicht auf eigene Adressbereiche beschränken")
@click.option('--manager/--no-manager', 'listopt_mgr', default=False,
              help="Betreuerliste pro Bereich ausgeben")
@click.option('--name', 'name', default=None, help="Name des Adressbereiches")
@click.option('--regexp', 'name_regexp', help="Regulärer Ausdruck des Suchmusters für Adressbereichsnamen")
@click.option('--oe', help="eindeutiger kurzer Name der Organisationseinheit")
@click.option('--subnet', 'subnet_cidr_mask',
              help="CIDR-Ausdruck für Such-oder Ausgabefunktionen, ggf. in Verbindung mit CIDR-Operator")
@click.pass_context
def list_range(context,
               filter_subnet_zero_mask,
               listopt_bcds,
               listopt_domains,
               listopt_gateways,
               listopt_global,
               listopt_mgr,
               name,
               name_regexp,
               oe,
               subnet_cidr_mask):
    logger.info("Called func list")
    params = dict()

    if filter_subnet_zero_mask is True:
        params['filter_subnet_zero_mask'] = filter_subnet_zero_mask
    if listopt_bcds is True:
        params['listopt_bcds'] = listopt_bcds
    if listopt_domains is True:
        params['listopt_domains'] = listopt_domains
    if listopt_gateways is True:
        params['listopt_gateways'] = listopt_gateways
    if listopt_global is True:
        params['listopt_global'] = listopt_global
    if listopt_mgr is True:
        params['listopt_mgr'] = listopt_mgr
    if name is not None:
        params['name'] = name
    if name_regexp is not None:
        params['name_regexp'] = name_regexp
    if oe is not None:
        params['oe'] = oe
    if subnet_cidr_mask is not None:
        params['subnet_cidr_mask'] = subnet_cidr_mask

    logger.debug("Using options %s", params)
    data = query.dns_range_list(context.obj.cert, params=params)
    print(tabulate(data, headers='keys'))
